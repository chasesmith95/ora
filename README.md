# Ora
Ora is a decentralized application programming interface for blockchains with the specific aim of providing a universal light client for blockchain services. Ora provides a light client with an easy service interface to sending transactions and pulling data from different blockchains.  

## Get started 
To get started with Ora check out our API. For further information about Ora checkout the [website](www.orachain.io). 

## Generalized architecture 
There are several different components within the Ora ecosystem:

* **Ora light Client** 

* **Ora Router**

* **Ora Full Node**






